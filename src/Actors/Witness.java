package Actors;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import CourtRecord.Evidence;
import IO.CharacterSpriteReader;
import IO.DialogTemplateReader;
import IO.PlayerController;

public class Witness extends ScoreKeeper {
    final int MAX_HALFTRUTH_RANGE = 3;
    private int crossExamineIndex = 0;

    private Random randomizer;
    // main constructor
    public Witness(String name, PlayerController playerInput, int score, int accuracy, int persuasion, CharacterSpriteReader characterSpriteReader){
        super(name,playerInput,score,characterSpriteReader); 
        this.randomizer = new Random();
        this.accuracy = accuracy;
        this.persuasion = persuasion;
    }
    private int accuracy;
    private int persuasion;
    private Statement[] statements;
    private String[] formattedStatements;

    // Generates a random number from 1 to 100 
    public boolean isLying(){
        return randomizer.nextInt(100)>accuracy;
    }
    // Generate the statements for the witness based on the given court record
    public void setStatements(Evidence[] courtRecord){
        int halfTruthCount = randomizer.nextInt(MAX_HALFTRUTH_RANGE)+1+persuasion/100;         
        Statement[] originalStatements = new Statement[courtRecord.length];
        // generate lies
        if(isLying()){
            int lieCount = ((100-accuracy)*courtRecord.length)/100;//the accuracy determines the amount of lies
            generateLies(originalStatements,courtRecord,lieCount);
        }
        // generate truths in remaining fields
        generateTruths(originalStatements,courtRecord);

        // now make the original statements a list, so that you can "pepper" in half truths
        ArrayList<Statement> statementsList = new ArrayList<Statement>();
        Collections.addAll(statementsList,originalStatements);
        generateHalfTruths(statementsList, courtRecord, halfTruthCount);
        // set the statements
        statements = new Statement[statementsList.size()];
        statements = statementsList.toArray(statements);
        generateFormattedStatements();
    }  
    // generate lies in random indexes 
    private void generateLies(Statement[] statements, Evidence[] courtRecord, int lieCount){
        generateStatementsInRandomIndexes(statements, lieCount, StatementType.Lie, courtRecord);
    }
    // geerate truths in remaining slots
    private void generateTruths(Statement[] statements, Evidence[] courtRecord){
        for(int i = 0; i<statements.length;i++){
            if(statements[i]==null){
                statements[i] = new Statement(courtRecord[i], StatementType.Truth);
            }
        }
    }
    // pepper in half truths in a stattements list
    private void generateHalfTruths(List<Statement> statements, Evidence[] courtRecord, int halfTruthCount){
        for(int i = 0; i<halfTruthCount;i++){
            int evidenceIndex = randomizer.nextInt(courtRecord.length);
            int listIndex = randomizer.nextInt(statements.size());
            statements.add(listIndex,new Statement(courtRecord[evidenceIndex],StatementType.HalfTruth));
        }
    }
    // Gets a statement by index
    public Statement getStatement(int index){
        return statements[index];
    }  
   
    // Generate a type of statement inside the statements array, but each index is random
    private void generateStatementsInRandomIndexes(Statement[] statements, int count, StatementType statementType, Evidence[] courtRecord){
        for(int i = 0; i<count; i++) {
            int randomIndex = getRandomStatementIndex(statements);
            int courtIndex = randomIndex%courtRecord.length;
            Statement statement = new Statement(courtRecord[courtIndex],statementType);
            statements[randomIndex] = statement;            
        }
    }
    // Generate an index for a random statement 
    private int getRandomStatementIndex(Statement[] statements){
        int availableStatements = 0;
        for(int i=0;i<statements.length;i++){
            if(statements[i]==null){
                availableStatements++;
            }
        }
        // if theres no more statements then throw an exception
        if(availableStatements==0){
            throw new IllegalStateException("Can't get random statement in witness.");
        }        
        // try to add the random index. if the entry is full, add one  
        int randomIndex = randomizer.nextInt(availableStatements);
        for(int i = 0; i<statements.length; i++){
            if(statements[randomIndex]!=null){
                randomIndex = (randomIndex+1)%statements.length;
                continue;
            }            
        }
        return randomIndex;
    }
    // Take the existing statements and generate them into text by getting the template data
    private void generateFormattedStatements(){
        // generate text from statements
        formattedStatements= new String[statements.length];
        DialogTemplateReader templateReader = new DialogTemplateReader();
        for(int i = 0; i<statements.length; i++){
            Statement statement = statements[i];
            formattedStatements[i] = templateReader.getRandomTemplate(statement.getEvidenceType(), statement.getValue());
        }
    }
    public String getFormattedStatement(int index){
        return formattedStatements[index];
    }
    // Make the witness state the statements out loud. (Converts all statements into readable dialog)
    public String[] sayStatements(){
        // Speak out the statements
        speak(formattedStatements);
        return formattedStatements;
    }
    public Statement getNextStatement(){
        if(crossExamineIndex==0){
            playerInput.showCrossExaminationGraphic();
        }
        Statement statement = getStatement(crossExamineIndex);
        speak(getFormattedStatement(crossExamineIndex)); 
        return statement;
    }
    public void respond(){
        Statement statement = getStatement(crossExamineIndex);
        // penalize witness if its a lie
        if( statement.getStatementType()==StatementType.Lie){
            penalize();
        }
        // if they guessed right, witness responds        
        String witnessResponse = statement.getStatementType()==StatementType.Truth ? "Y-yeah! That's right!" : "O-oh... Sorry! I didn't mean that...";
        speak(witnessResponse);
        crossExamineIndex++;
                
    }
    public boolean isCrossExamining(){
        return crossExamineIndex<statements.length;
    }
}
