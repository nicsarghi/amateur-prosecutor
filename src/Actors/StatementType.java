package Actors;
// Enum for statement types. Detmines if a witness is telling a lie, a truth or a halftruth.
public enum StatementType {
    Lie("lying"),
    Truth("telling the truth"),
    HalfTruth("not telling the entire truth");

    StatementType(String playerResponse){
        this.playerResponse= playerResponse;
    }
    // Used to store what the player would respond when they respond to a witness in-game
    private String playerResponse;
    public String getPlayerResponse(){
        return this.playerResponse;
    }

}
