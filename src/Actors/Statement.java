package Actors;
import CourtRecord.Evidence;

public class Statement {
    private Evidence baseEvidence;
    private String falsifiedValue;
    private StatementType statementType;
    // Create a statement with the evidence and the type of statement
    public Statement(Evidence baseEvidence, StatementType statementType){
        this.baseEvidence = baseEvidence;
        this.falsifiedValue = baseEvidence.getFalsitude();
        this.statementType = statementType;
    }
    // Get the value of the statement based on its type
    public String getValue(){
        switch (statementType) {
            case Truth:
                return baseEvidence.getValue();        
            case Lie:
                return falsifiedValue;
            case HalfTruth:
                return baseEvidence.getReducedValue();   
        }
        throw new IllegalStateException("Can't get value for statement.");
    }
    // Get the type of statement
    public StatementType getStatementType(){
        return statementType;
    }
    // Get the type of evidence
    public String getEvidenceType(){
        return baseEvidence.getType();
    } 

}
