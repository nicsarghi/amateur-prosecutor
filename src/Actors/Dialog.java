package Actors;
public class Dialog {
    private int currentLine;
    private String[] lines;
    private String speaker;
    private Actor actor;

    // Constructor for speaker-less dialog
    public Dialog(String[] lines){
        if(lines.length < 1){
            throw new IllegalArgumentException("No lines present in dialog.");
        }
        this.lines = lines;
    }
    // Constructor for dialog with a speaker
    public Dialog(String speaker, String[] lines){
        this(lines); // avoid code repitition
        this.speaker = speaker;
    }
    // Constructor for dialog with an actor
    public Dialog(Actor actor, String[] lines){
        this(lines); // avoid code repitition
        this.speaker = actor.getName();
        this.actor = actor;
    }
    // Appends appends a prefix to the speaker string
    public String getSpeakerPrefixIfExists(){
        return speaker == null ? "" : speaker+": ";
    } 
    // Increase the line count. Checks it exceeds the dialog size
    public void next(){
        if(currentLine>=lines.length)
        {
            throw new IllegalStateException("Cannot call next when reached max line count "+lines.length);
        }
        currentLine ++;
    }
    public String getSpeaker(){
        return speaker;
    }
    public Actor getActor() {
        return actor;
    }
    public String getLine(){
        return lines[currentLine];
    }
    public int getCount(){
        return lines.length;
    }
    public boolean isFinished(){
        return currentLine+1>=getCount();
    }
}
