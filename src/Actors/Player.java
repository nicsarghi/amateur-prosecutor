package Actors;
import IO.CharacterSpriteReader;
import IO.PlayerController;

public class Player extends ScoreKeeper{
    public Player(String name, PlayerController playerInput, int score, CharacterSpriteReader characterSpriteReader){
        super(name,playerInput,score,characterSpriteReader);
    }
    // Prompt the user to respond to a proposal by the witness
    public void objectProposal(StatementType guess){
        // Evaluate on whether the user was correct on their response
        String sentenceFinish = guess.getPlayerResponse();
        speak("Your honor, I think the witness is "+sentenceFinish); //response
    }
}
