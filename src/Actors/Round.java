package Actors;
import java.util.Random;

import CourtRecord.Evidence;
import CourtRecord.TimeOfDeath;
import CourtRecord.Victim;
import CourtRecord.Weapon;
import IO.CharacterNameReader;
import IO.CharacterSpriteReader;
import IO.EvidenceListReader;
import IO.PlayerController; 

public class Round {
    final private int PLAYER_SCORE = 5;

    private Random randomizer;
    private Witness[] witnesses;
    private Evidence[] courtRecord;
    private CharacterNameReader characterNameReader;
    private CharacterSpriteReader characterSpriteReader;

    private Player player;
    private PlayerController playerInput;
    private int witnessCounter = 0;

    // Constructor method for the round
    public Round(PlayerController playerInput, int witnessCount){
        if(witnessCount<1){
            throw new IllegalArgumentException("Witness count cannot be under 1");
        }
        this.playerInput = playerInput;
        playerInput.setRound(this);
        this.characterNameReader = new CharacterNameReader(); // first get the character name generator to gen victim + witnesses
        this.characterSpriteReader = new CharacterSpriteReader();
        randomizer = new Random();
        generateCourtRecord(); // then get the court record
        generateWitnesses(witnessCount); // you need 
        player = new Player(playerInput.getName(), playerInput, PLAYER_SCORE, characterSpriteReader);
    }
    // Generate the witnesses for a round 
    private void generateWitnesses(int witnessCount){
        witnesses = new Witness[witnessCount];
        for(int i = 0; i< witnessCount; i++){
            // accuracy is determined randomly
            // the last witness should be a liar 
            int accuracy = i == witnessCount-1 ? 0 : randomizer.nextInt(100); 
            // persuasiveness is random, but the range increases with each witness
            int persuasion = randomizer.nextInt((int)(((double)i+1)/witnessCount*100));
            // create a witness and init the statements
            Witness witness = new Witness(characterNameReader.getRandomCharacterName(),playerInput,courtRecord.length/2,accuracy,persuasion,characterSpriteReader);
            witness.setStatements(courtRecord);
            witnesses[i] = witness;
        }
    }
    // Generate the court record for a round
    private void generateCourtRecord(){
        // Get an evidence list reader to generate some of the evidence
        EvidenceListReader reader = new EvidenceListReader();    
        // The court record consists of 3 pieces of main evidence:
        // the victim, the time of death and the murder weapon.
        courtRecord = new Evidence[3];
        courtRecord[0] = new Victim(characterNameReader.getRandomCharacterName());
        courtRecord[1] = new TimeOfDeath();
        //generate the weapon 
        String[] weapon = reader.getRandomEvidenceArguments("Weapon"); // you want the weapon type + the weapon name 
        courtRecord[2] = new Weapon(weapon[0],reader,weapon[2]);
    }
    // Introduce the round
    public void introduction(){
        String victim = courtRecord[0].getValue();

        String[] introText = new String[]{"Court is now in session for the trial of the murder of "+victim };
                                        //"The defendant of this case is accused of this murder.",
                                        //"Your job is to play the role of the defense and find the true culprit. Good luck"};
        Dialog dialog = new Dialog(introText);
        playerInput.showDialog(dialog);                               
    } 
    public boolean isTestifying(){
        return witnessCounter < witnesses.length && !player.hasLost();
    }
    public Player getPlayer() {
        return player;
    } 
    // gets the witness by index
    public Witness[] getWitnesses(){
        return witnesses;
    }
    // Gets the court record
    public Evidence[] getCourtRecord() {
        return courtRecord;
    }      
    /// Get next witness
    public Witness getNextWitness(){
        Witness nextWitness = witnesses[witnessCounter];
        showWitness(nextWitness);
        witnessCounter++;
        return nextWitness;
    } 
  
    // determine if the culprit lost or the player lost 
    public boolean playerHasWon(Witness lastWitness){
        if(player.hasLost()){
            return false;
        }else if(lastWitness.hasLost()){
            return true;
        }
        throw new IllegalStateException("The last witness has not lost, nor has the player won.");
    }
    // Displays the witness' testimony and allow the player to examine them
    private void showWitness(Witness witness){
        playerInput.showTestimonyGraphic();
        // first show the testimony in total
        witness.sayStatements();
    }

}
