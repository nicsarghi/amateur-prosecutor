package Actors;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import CourtRecord.*;
import IO.CharacterSpriteReader;
import IO.ConsoleController;

public class WitnessTests {
    // this exists to do manual testing 
    // since i need to know the output
    public static void main(String[] args){
        testLieDialog();
    }
    public static void testStatementDialog(){
        CharacterSpriteReader characterSpriteReader = new CharacterSpriteReader();
        Evidence[] courtRecord= new Evidence[2];
        courtRecord[0] = new Victim("John Smith");
        courtRecord[1] = new TimeOfDeath();

        Witness accurateWitness = new Witness("Mercy", new ConsoleController(), 3, 100, 0, characterSpriteReader);
        accurateWitness.setStatements(courtRecord);
        accurateWitness.sayStatements();
    }
    public static void testLieDialog(){
        CharacterSpriteReader characterSpriteReader = new CharacterSpriteReader();
        Evidence[] courtRecord= new Evidence[2];
        courtRecord[0] = new Victim("John Smith");
        courtRecord[1] = new TimeOfDeath();

        Witness accurateWitness = new Witness("Winsor", new ConsoleController(), 3, 0, 0, characterSpriteReader);
        accurateWitness.setStatements(courtRecord);
        accurateWitness.sayStatements();
    }
    // actual unit tests below
    @Test
    public void TestLieConstructor(){
        CharacterSpriteReader characterSpriteReader = new CharacterSpriteReader();
        Witness witness = new Witness("Kelly", new ConsoleController(), 3, 100, 0, characterSpriteReader);
        assertEquals(false,witness.isLying());
    }
    @Test
    public void TestStatementGenerator(){
        CharacterSpriteReader characterSpriteReader = new CharacterSpriteReader();
        Witness accurateWitness = new Witness("Mercy", new ConsoleController(), 3, 100, 0, characterSpriteReader);
        assertEquals(false,accurateWitness.isLying());

        Evidence[] courtRecord= new Evidence[2];
        courtRecord[0] = new Victim("John Smith");
        courtRecord[1] = new Victim("John Doe");
        accurateWitness.setStatements(courtRecord);
        assertEquals("John Smith",accurateWitness.getStatement(0).getValue());
        assertEquals("John Doe",accurateWitness.getStatement(1).getValue());
    }
    @Test
    public void TestLyingWitness(){
        CharacterSpriteReader characterSpriteReader = new CharacterSpriteReader();
        Witness liar = new Witness("Beel", new ConsoleController(), 3, 0, 0, characterSpriteReader);

        Evidence[] courtRecord= new Evidence[2];
        courtRecord[0] = new Victim("John Smith");
        courtRecord[1] = new Victim("John Doe");
        liar.setStatements(courtRecord);
        if(liar.getStatement(0).getStatementType()==StatementType.Truth){ // ONE of them is a lie
            System.out.println(liar.getStatement(0).getValue());
            assertNotEquals("John Smith", liar.getStatement(0).getValue());
            assertEquals("John Doe", liar.getStatement(1).getValue());
        }else{
            System.out.println(liar.getStatement(1).getValue());
            assertEquals("John Smith", liar.getStatement(0).getValue());
            assertNotEquals("John Doe", liar.getStatement(1).getValue());
        }
    }
}
