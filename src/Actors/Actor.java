package Actors;

import IO.CharacterSpriteReader;
import IO.PlayerController;
import UI.ImageLoader;

public class Actor {
    private String name;
    protected PlayerController playerInput;
    protected ImageLoader icon;
    
    // Create an Actor for the game based on the name 
    public Actor(String name, PlayerController playerInput, CharacterSpriteReader spriteReader){
        this.name=name;
        this.playerInput = playerInput;
        this.icon = spriteReader.getRandomSprite();
    }
    public String getName() {
        return name;
    }
    // Speak allows the Actor to communicate to the user using text. Creates a dialog with their name on it
    public void speak(String[] text){
        Dialog dialog = new Dialog(this,text);
        playerInput.showDialog(dialog);
        //dialog.show();
    }
    // Overload variant for only saying one line
    public void speak(String text){
        String[] textArray = new String[]{text};
        speak(textArray);
    }
    // Retrieve the icon for the actor
    public ImageLoader getIcon() {
        return icon;
    }
}
