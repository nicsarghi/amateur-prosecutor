package Actors;

import IO.CharacterSpriteReader;
import IO.PlayerController;

public class ScoreKeeper extends Actor {
    // constructor for the score keeper
    public ScoreKeeper(String name, PlayerController playerInput, int score, CharacterSpriteReader spriteReader){
        super(name,playerInput,spriteReader);
        if(score<1){
            throw new IllegalArgumentException("Score must be above 0");
        }
        this.score = score;
    } 
    // keep track of penalties in score keeper
    private int score;
    // returns true if the score is at 0
    public boolean hasLost(){
        return score <= 0;
    }
    // redact 1 point from the score
    public void penalize(){
        score--;
    }   
}
