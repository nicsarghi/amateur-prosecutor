import java.util.Scanner;

import Actors.Dialog;
import Actors.Round;
import Actors.Statement;
import Actors.StatementType;
import Actors.Witness;
import IO.ConsoleController;

public class ConsoleApplication {
    public static void main(String[] args) {
        ConsoleController controller = new ConsoleController();
        playRound(controller);
    }
    public static void playRound(ConsoleController controller){        
        // create round and show court record
        Round round = new Round(controller,getWitnessCount()); 
        controller.showCourtRecord();
        // loop through witnesses and cross examine them
        Witness lastWitness;
        do{
            lastWitness = round.getNextWitness();
            crossExamineWitness(controller,round,lastWitness);
        }while(round.isTestifying());
        // determine who wins
        boolean playerHasWon = round.playerHasWon(lastWitness);
        controller.finishingSequence(playerHasWon, lastWitness);
    }
    public static void crossExamineWitness(ConsoleController controller, Round round, Witness witness){
        // cross examine witness
        do {
            // get the next statement
            Statement witnessStatement = witness.getNextStatement();
            controller.prepareObjection();
            // get the player's guess. make them output it to the console
            StatementType guess = controller.getObjection();
            boolean isCorrect = witnessStatement.getStatementType() == guess;
            round.getPlayer().objectProposal(guess);
            
            if(!isCorrect){ // if the player is not correct, then they get penalized 
                controller.showDialog(new Dialog(new String[]{"The court rejects your proposal."}));
                round.getPlayer().penalize();
            }else{ // or the witness gets penalized
                witness.respond(); 
            } // repeat this until the player runs out of turns or the witness is done
        } while (witness.isCrossExamining() && !round.getPlayer().hasLost()); 
    }

    // gets the witness count from the user
    public static int getWitnessCount(){
        System.out.print("Witness count: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
