import IO.UIController;
import javafx.application.*;
import javafx.stage.*;
import UI.*;

public class ProsecutorApplication extends Application {
    private UIController uiController=new UIController();
    private final int SCENE_WIDTH = 750;
    private final int SCENE_HEIGHT = 500;
    @Override
    public void start(Stage stage){      
        //scene is associated with container, dimensions
        StartScene scene = new StartScene(SCENE_WIDTH,SCENE_HEIGHT,uiController);
        SceneController sceneController = new SceneController(stage);
        uiController.setSceneController(sceneController);

        //associate scene to stage and show
        stage.setTitle("Amateur Prosecutor");
        stage.setScene(scene); 
        stage.show();          
    }
    // Main method starts the round for now
    public static void main(String[] args) {
        Application.launch(args);

    }
}