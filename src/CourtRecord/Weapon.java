package CourtRecord;
import IO.EvidenceListReader;

public class Weapon implements Evidence {
    private String value;
    private String type;
    private EvidenceListReader evidenceReader;
    // Create a weapon based on the weapon type and value. The evidence reader is used to find fake weapons.
    public Weapon(String value, EvidenceListReader evidenceReader, String type){
        this.value = value;
        this.type = type;
        this.evidenceReader = evidenceReader;
    }
    // Get the true value of the weapon
    public String getValue() {     
        return this.value;
    }
    // Generates a fake weapon by looking through the evidence reader
    public String getFalsitude() {
        final int RETRY_COUNT = 5;
        int retries = 0;
        do{
            // basically find weapon that's NOT the current weapon
            String found = evidenceReader.getRandomEvidence("Weapon");
            if(!found.equals(value)){
                return found;
            }
            retries++; // have a retry count so it wont look for random evidence forever 
        }while(retries < RETRY_COUNT );       
        throw new NullPointerException("Could not get other weapon type.");
    }  
    //Get the classname
    public String getType(){
        return "Weapon";
    }
    // A vague version of the truth would be to return what type of object it is.
    public String getReducedValue() {        
        return type+" object...";
    }     
}
