package CourtRecord;

import java.util.Random;

public class TimeOfDeath implements Evidence {
    private static final int HOURS = 24;
    private static final int MINUTES = 59;

    private int hour;
    private int minutes;
    private Random randomizer;
    private String[] reductions;
    // Initialize the time of death
    public TimeOfDeath(){
        this.randomizer = new Random();
        this.hour = randomizer.nextInt(HOURS);
        this.minutes = randomizer.nextInt(MINUTES);
        setReductions();
    }
    // Reductions where the witness gives a vague or insufficient desciption of the time
    private void setReductions(){
        reductions = new String[]{
            "maybe around "+getHour(),
            "....I forgot.",
            "it well it HAD to be past "+getHour(),
        };
    }
    // Gets the time of death in hours and minutes
    public String getValue() {
        return getHour()+":"+getMinutes();
    }
    // Returns and appends 0 to minutes if it's a single digit
    private String getMinutes(){
        return minutes < 10 ? "0"+minutes : ""+minutes;
    }
    // Returns the hour
    private String getHour(){
        return ""+hour;
    }
    // Gets a false version of the evidence. Adds a maximum of 5 hours to the actual hour 
    public String getFalsitude() {
        final int DISCREPANCY = 5;
        int fakeHour = (this.hour+1+randomizer.nextInt(DISCREPANCY))%HOURS; // use modulus to not get values over 24
        return fakeHour+":"+getMinutes();
    }  
    // Returns the stringified version of this class name (used for file io)
    public String getType(){
        return "TimeOfDeath";
    }
    // Returns a vague/incoherent version of the true value by grabbing a random phrase from the reductions array
    public String getReducedValue() {
        int index = randomizer.nextInt(reductions.length);
        return reductions[index];
    }     
}
