package CourtRecord;

import java.util.Random;

public class Victim implements Evidence{
    private String value;
    private Random randomizer;
    // Constructor to initialize the randomizer
    public Victim(String value){       
        randomizer = new Random();
        this.value = value;           
    }
    // Gets the true value of the evidence
    public String getValue(){
        return value;
    }  
    // Get the class name 
    public String getType(){
        return "Victim";
    } 
    // Retun a fake version of the witness name by replacing random characters in the name with other ones.
    public String getFalsitude(){
        final int REPLACE_RATE = 40; // Percentage to determine dif the character is replaced
        final int CHARACTER_RANGE = (int)'z'-(int)'a';
        // Convert the value into a charArray in orde to loop through it
        char[] characters = value.toCharArray(); 
        StringBuilder replaced = new StringBuilder();
        for(char character: characters){
            // determine if the character is replaced
            boolean isRandom = randomizer.nextInt(100)<REPLACE_RATE;
            if(isRandom){ // if it is, determine if its lowerCase, replace it, then set the new value to lowercase/uppercase   
                boolean isLowerCase = Character.isLowerCase(character);                
                int randomLetter = (int)'a'+randomizer.nextInt(CHARACTER_RANGE);                
                character = isLowerCase ? (char)randomLetter : Character.toUpperCase(character);
            }
            // add the replaced or conserved character to the string
            replaced.append(character);
        }   
        return replaced.toString();
    }
    // Return a vague description of the witness' name
    public String getReducedValue() {
        // split it by spaces
        String[] splitNames = value.split("\\s");
        // reduce it into the firstname or last name
        String reduced = splitNames[randomizer.nextInt(splitNames.length)];
        return "Mr or Mrs... "+reduced; // make it seem more vague by adding Mrs or Mrs, as if the witness is unsure
    }
}
