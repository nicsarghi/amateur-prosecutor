package CourtRecord;

public interface Evidence {
    String getValue();
    String getFalsitude();
    String getType();
    String getReducedValue();
}
