package CourtRecord;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

import IO.EvidenceListReader;

public class EvidenceTests {
    @Test
    public void TestTrueVictim(){
        Victim v = new Victim("Jane Doe");
        assertEquals("Jane Doe",v.getValue()); 
    } 
    @Test
    public void TestFalseVictim(){
        Victim v = new Victim("Jane Doe");
        String falsitude = v.getFalsitude();
        System.out.println(falsitude);
        assertNotEquals("Jane Doe",falsitude); 
    } 
    @Test
    public void TestMurderWeapon(){
        EvidenceListReader evidenceReader = new EvidenceListReader();
        Weapon w = new Weapon("Knife",evidenceReader,"Sharp");
        assertEquals("Knife",w.getValue());
    }  
    @Test
    public void TestFakeWeapon(){
        EvidenceListReader evidenceReader = new EvidenceListReader();
        Weapon w = new Weapon("Knife",evidenceReader,"Sharp");
        String falsitude = w.getFalsitude();
        System.out.println(falsitude);
        assertNotNull(falsitude);
        assertNotEquals("Knife",falsitude);
    }     
    @Test
    public void TestFakeTimeofDeath(){
        TimeOfDeath time = new TimeOfDeath();
        String falsitude = time.getFalsitude();
        
        System.out.println(falsitude);
        System.out.println(time.getValue());
        assertNotEquals(time.getValue(),falsitude);
    } 
}
