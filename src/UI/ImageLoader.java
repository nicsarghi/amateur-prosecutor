package UI;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

// class used to load an image from a path and retrieve an image view if needed 
public class ImageLoader {
    private Image image;
    // loads an image from the assets folder
    public ImageLoader(String assetFilePath){
        try{ // try to get the file
            FileInputStream inputstream = new FileInputStream("src/Assets/"+assetFilePath); 
            image = new Image(inputstream); 

        }catch(FileNotFoundException exception){ // if not, then throw a runtime exception
            throw new IllegalArgumentException("Invalid path name: "+assetFilePath);
        }                
    }
    // generates an imageview from the image object. allows the user to set the preserve ratio automatically
    public ImageView getView(boolean preserveAspectRatio){
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(preserveAspectRatio);
        return imageView;
    }
}
