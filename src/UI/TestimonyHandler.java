package UI;

import Actors.Dialog;
import Actors.Round;
import Actors.Statement;
import Actors.StatementType;
import Actors.Witness;
import IO.UIController;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
// keeps track of the states inside a round. everytime the dialog is clicked, it pushes the round into a new state 
public class TestimonyHandler implements EventHandler<MouseEvent> {
    public enum TestimonyState{Introducing,Testifying,CrossExamining,PlayerResponding,GameResponding,Finished}

    private Round round;
    private Witness lastWitness;
    private Statement lastStatement;
    private UIController uiController;
    private TestimonyState state;

    public TestimonyHandler(Round round,UIController uiController){
        this.round = round;
        this.uiController = uiController;
        this.state = TestimonyState.Introducing;
    }
    // the handle method that triggers when the dialog finishes
    @Override
    public void handle(MouseEvent event) {
        switch (state) {
            case Introducing: // the introduction dialog only happens once and progresses to the testimony
                state = TestimonyState.Testifying;
                lastWitness = round.getNextWitness();
                break;   
            case Testifying:
                state = TestimonyState.CrossExamining;  // once a witness is done testifying, you cross examine them 
                lastStatement = lastWitness.getNextStatement();
                break;
            case CrossExamining: // once a witness has said their statement for the cross examination, the player can respond
                state = TestimonyState.PlayerResponding;   
                uiController.prepareObjection();
                break;      
            case PlayerResponding: // once the player responds, the round will respond 
                handlePlayerResponding();
                break;  
            case GameResponding: 
                // once the game responds, then it will make the witness repeat itself, 
                //move to the next witness or finish the round
                handleGameResponding();
                break;
            case Finished:
                //once the round is finished, then it can conclude and terminate
                uiController.endRound();
                break;
        }
    }
    // helper method for the PlayerResponding enum. 
    //Triggers when the player actor has objected to the witness' statement
    public void handlePlayerResponding(){
        // get whether or not the player is correct
        StatementType guess = uiController.getObjection();
        boolean isCorrect = guess ==  lastStatement.getStatementType();
        state = TestimonyState.GameResponding;
        // if they're not correct, penalize them
        if(!isCorrect){
            uiController.showDialog(new Dialog(new String[]{"The court rejects your proposal."}));
            round.getPlayer().penalize();
        }else{ // otherwise make the witness respond
            lastWitness.respond(); 
        }
    }
    // helper method for the GameResponding enum. 
    //Triggers when the game has finished responding to the player's objection
    public void handleGameResponding(){
        // the round continues if the witness is cross examining or if the round is still testifying, AND if the player hasnt lost
        boolean roundIsContinuing = (round.isTestifying() || lastWitness.isCrossExamining())&&!round.getPlayer().hasLost();
        // basically, if the 
        if (roundIsContinuing){
            boolean isCrossExamining = lastWitness.isCrossExamining(); 
            state = isCrossExamining ? TestimonyState.CrossExamining : TestimonyState.Testifying;
            if(isCrossExamining){ // if they're cross examining still, keep doing that 
                lastStatement = lastWitness.getNextStatement();
            }else{ // if not, move to the next witness
                lastWitness = round.getNextWitness();
            }
            
        }else{ // this means the round is over
            boolean playerHasWon = round.playerHasWon(lastWitness);
            state = TestimonyState.Finished;   
            // start the finishing dialog sequence,
            uiController.finishingSequence(playerHasWon, lastWitness);               
        }
    }
    
}
