package UI;

import Actors.StatementType;
import IO.UIController;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
// the objection box is what the 
public class ObjectionBox extends BottomBox {
    private UIController uiController;
    public ObjectionBox(Group container, UIController uiController){
        super(container);
        this.uiController = uiController;
        addGui();
    }
    // adds the gui elements to the box
    @Override
    protected void addGui() {
        // add the hbox container
        HBox container = new HBox();
        getChildren().add(container);
        // init the container
        container.setSpacing(20);
        container.getStyleClass().add("dialog-box");
        container.setPrefSize(getWidth(), getHeight());
        container.setAlignment(Pos.CENTER);
        // add buttons to the container
        addButtons(container);  
        setVisible(false);   
    }
    // adds the buttons inside of the box
    protected void addButtons(HBox container){
        // add a button each for a truth, half truth and lie 
        Button truthButton = new Button("Truth");
        truthButton.setOnAction(new ObjectionActionHandler(StatementType.Truth, uiController));

        Button lieButton = new Button("Lie");
        lieButton.setOnAction(new ObjectionActionHandler(StatementType.Lie, uiController));
        
        Button halfTruthButton = new Button("Press for more info");
        halfTruthButton.setOnAction(new ObjectionActionHandler(StatementType.HalfTruth, uiController));

        container.getChildren().addAll(truthButton,lieButton,halfTruthButton);
    }
}
