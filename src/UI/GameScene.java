package UI;

import IO.UIController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public abstract class GameScene extends Scene {
    protected UIController uiController;
    // constructor initializes the scene + the ui controller
    public GameScene(double width, double height, UIController uiController) {
        super(new Group(),width,height);
        this.uiController = uiController;
    }
    // used to draw the gui
    protected abstract void addGui();   
    // called after the scene is switched
    public abstract void show();
    // background image method
    protected void addBackgroundImage(Group container,ImageLoader background){
        ImageView imageView = background.getView(true);
        imageView.setFitHeight(getHeight());  
        container.getChildren().add(imageView);
    }
    // style loader method
    protected void loadStyle(String fileName){
        String path = "CSS/"+fileName;
        getStylesheets().add(path);
        setFill(Color.BLACK);
    }    
}
