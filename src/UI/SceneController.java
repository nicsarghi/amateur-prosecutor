package UI;

import java.util.HashMap;
import java.util.Map;

import javafx.stage.Stage;

public class SceneController {  
    private Stage stage;
    private Map<String,GameScene> scenes = new HashMap<String,GameScene>();
    // add a scene controller
    public SceneController(Stage stage){
        this.stage = stage;
    }  
    // adds a scene to the controller, with the index being the class name
    public void addScene(GameScene scene){
        String key = scene.getClass().getSimpleName();
        scenes.put(key,scene);
    }
    // removes a scene from the controller
    public void removeScene(String key){
        scenes.remove(key);
    }
    // gets the stage's current scene
    public GameScene getCurrentScene(){
        return (GameScene)stage.getScene();
    }
    // adds a new scene and shows it
    public void showNewScene(GameScene scene){
        String key = scene.getClass().getSimpleName();
        addScene(scene);
        showScene(key);        
    }
    // shows the scene
    public void showScene(String key){
        GameScene scene =  scenes.get(key);
        stage.setScene(scene);
        scene.show();
    }
}
