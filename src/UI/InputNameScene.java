package UI;
import IO.UIController;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class InputNameScene extends GameScene {
    private int MAX_WITNESS_COUNT = 10;
    private int MIN_WITNESS_COUNT = 1;

    private Text messageBox;
    private ImageLoader startScreenBackground;
    private TextField witnessCountField;
    // constructor for the scene
    public InputNameScene(double width, double height, UIController uiController, ImageLoader startScreenBackground) {
        super(width,height,uiController);
        this.startScreenBackground = startScreenBackground; 
        // add bg
        Group root = (Group)getRoot();
        addBackgroundImage(root);   
        // add gui   
        addGui();       
    }
    // add gui to the scene
    @Override
    protected void addGui() {
        loadStyle("namescreen-style.css");
        // init the main container
        Group root = (Group)getRoot();
        VBox mainContainer = new VBox();
        mainContainer.setPrefWidth(getWidth());
        mainContainer.setAlignment(Pos.CENTER);
        // add the gui contents
        addBackgroundImage(root);
        addTitleText(mainContainer);
        addInputBoxes(mainContainer);
        addMessageBox(mainContainer);
        addInputButton(mainContainer);
        // add main container to top layer of root
        root.getChildren().add(mainContainer);
    }
    // adds the title text class to the main container
    private void addTitleText(VBox mainContainer){
        Text title = new Text("Controls");
        title.getStyleClass().add("title");
        title.setFill(Color.WHITE);
        mainContainer.getChildren().add(title);
    }
    // adds the two input boxes to the main container
    private void addInputBoxes(VBox mainContainer){  
        VBox box = new VBox();
        box.setSpacing(4);
        box.setPadding(new Insets(4));
        TextField nameField = addInputFieldWithLabel(box, "Your name:");        
        uiController.setInput(nameField); 
        witnessCountField = addInputFieldWithLabel(box, "Witness count: ");

        mainContainer.getChildren().add(box);
    }
    // helper method to create an input field 
    private TextField addInputFieldWithLabel(VBox container,String labelText){
        // container to store the labe and field
        HBox inputContainer = new HBox();
        inputContainer.setAlignment(Pos.CENTER);
        container.getChildren().add(inputContainer);
        // create a label
        Text inputLabel = new Text(labelText);
        TextField field = new TextField(); // the the field
        // style it
        field.getStyleClass().add(".input-box");
        inputLabel.setFill(Color.WHITE);
        inputContainer.getChildren().addAll(inputLabel,field); 
        return field;
    }
    // add the "submit" utton for the scene that starts up the round
    private void addInputButton(VBox container){
        // create the button
        Button button = new Button("Set!");
        button.setPrefWidth(100);
        container.getChildren().add(button);
        // anonymous class for listening to input
        button.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent event){
                if(!validName()){ // exit if name is invlaid
                    return;
                }
                if(!validWitnessCount()){ // exit if witness count is invalid
                    return;
                }
                // get witness count and switch to new scene 
                int witnessCount = Integer.parseInt(witnessCountField.getText());
                RoundScene newScene = new RoundScene(getHeight(),uiController,witnessCount);
                uiController.getSceneController().showNewScene(newScene);    
            }
        });
    }
    // returns true if the inputted name is valid
    private boolean validName(){
        uiController.prepareName();
        String inputtedName = uiController.getName();
        // if its not empty
        if(inputtedName.isBlank()){
            setMessageText("Haha. Very funny. Now enter a name.");
            return false;
        }
        // if its alphanumeric
        if(inputtedName.matches(".*[^\\w\\s].*")){
            setMessageText("I said a name, not mumbled garbage. Enter something that's alphanumeric.");
            return false;
        } 
        return true;
    } 
    // returns true if the witness count is valid 
    private boolean validWitnessCount(){
        String input = witnessCountField.getText();
        // if its a number
        if(!isInteger(input)){
            setMessageText("Input A NUMBER for the witness count. Come on now.");
            return false;
        }
        int count = Integer.parseInt(input);
        // check if its below 1
        if(count<MIN_WITNESS_COUNT){
            setMessageText("Do you really expect me to count in negative witnesses?");
            return false;
        }  
        // check if its over 10 
        if(count>MAX_WITNESS_COUNT){
            setMessageText("Maximum witness count is 10. Don't make your life hell.");
            return false;
        }     
        return true;    
    }
    // helper method that returns true if the string is an interger
    private boolean isInteger(String value){
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    // sets the error message box text
    private void setMessageText(String text){
        messageBox.setText(text);
    }
    // creates a message box that tells the user to modify an invalid field 
    private void addMessageBox(VBox container){
        messageBox = new Text();
        messageBox.setFill(Color.WHITE);
        container.getChildren().add(messageBox);
    }
    //overload for adding a background image
    private void addBackgroundImage(Group container){
        addBackgroundImage(container, startScreenBackground);
    }
    @Override
    public void show() {   
        setFill(Color.BLACK);
    }
    
}
