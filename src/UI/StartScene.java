package UI;
import IO.UIController;
import javafx.event.*;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import javafx.scene.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;

public class StartScene extends GameScene{
    ImageLoader startScreenBackground; 
    public StartScene(double width, double height, UIController uiController) {
        super(width,height,uiController);
        // add bg
        Group root = (Group)getRoot();
        addBackgroundImage(root);
        show();
    }
    protected void addGui(){
        loadStyle("startscreen-style.css");
        Group root = (Group)getRoot();
        // create the vbox container
        VBox container = new VBox();
        container.setPrefWidth(getWidth());
        container.setAlignment(Pos.CENTER);
        // add all gui
        addTitle(container);
        addLabelText(container);
        // add container to root
        root.getChildren().add(container);
        // wait for the user to switch scenes
        listenForInput();
    }
    private void addTitle(VBox container){
        ImageLoader imageLoader = new ImageLoader("startscreen-title.png");
        ImageView titleImage = imageLoader.getView(true);
        titleImage.setFitHeight(250);  

        container.getChildren().add(titleImage);
    } 
    private void addBackgroundImage(Group container){
        startScreenBackground = new ImageLoader("startscreen-bg.jpeg");
        addBackgroundImage(container,startScreenBackground);
    }
    private void addLabelText(VBox container){
        Text text = new Text("Press any key to continue");
        text.getStyleClass().add("start-text");
        container.getChildren().add(text);
    }
    private void listenForInput(){    
        setOnKeyPressed(new EventHandler<KeyEvent>(){
            public void handle(KeyEvent keyEvent) {
                InputNameScene newScene = new InputNameScene(getWidth(),getHeight(),uiController,startScreenBackground);
                uiController.getSceneController().showNewScene(newScene);
            }            
        });
    }
    @Override
    public void show() {
        addGui();        
    }
}
