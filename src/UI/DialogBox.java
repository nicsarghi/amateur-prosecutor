package UI;
import Actors.Dialog;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class DialogBox extends BottomBox {
    private final int TEXT_PADDING_X = 75;
    private final int TEXT_PADDING_Y = 25;
    private final int SCROLL_DURATION = 50;

    private Label textBox;
    private Label speakerBox;
    private EventHandler<MouseEvent> onFinishedHandler;

    public DialogBox(Group container){
        super(container);
        addGui();
    }
    // define an event handler class for when the dialog has finished. 
    public void setOnFinishedDialog(EventHandler<MouseEvent> onFinishedHandler){
        this.onFinishedHandler=onFinishedHandler;
    }
    // adds the gui to the box
    protected void addGui(){
        // main label box
        textBox = new Label();
        textBox.setTextFill(Color.WHITE);
        textBox.getStyleClass().add("dialog-box");
        textBox.setPrefSize(getWidth(), getHeight());
        textBox.setPadding(new Insets(TEXT_PADDING_Y,TEXT_PADDING_X,TEXT_PADDING_Y,TEXT_PADDING_X));
        textBox.setAlignment(Pos.TOP_LEFT);
        textBox.setWrapText(true);
        // add speaker label
        speakerBox = new Label();
        speakerBox.setAlignment(Pos.CENTER);
        speakerBox.setTextFill(Color.WHITE);
        speakerBox.getStyleClass().add("dialog-speaker");
        speakerBox.setTranslateY(-25);
        speakerBox.setTranslateX(-10);
        speakerBox.setMinWidth(150);
        speakerBox.setMinHeight(40);
        speakerBox.setVisible(false);

        getChildren().addAll(textBox,speakerBox);      
    }
    // sets the speaker sub-box if its not null
    private void setSpeaker(String speaker){
        boolean isVisible = speaker!=null;
        // show speaker box
        speakerBox.setVisible(isVisible);
        if(isVisible){
            speakerBox.setText(speaker);
        }
    }
    // adds a timeline that plays a typing animation
    private Timeline generateTypingAnimation(String text){
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        // the first keys are the typing animation itself
        for(int i=0;i<text.length();i++){
            Label loadedBox = textBox; 
            final int index = i; // avoids compiler err with scope
            Duration duration = Duration.millis(SCROLL_DURATION*index); 
            // create a new keyframe for each character added
            KeyFrame key = new KeyFrame(duration, new EventHandler<ActionEvent>(){
                public void handle(ActionEvent event) {         
                    // gets the substring based on i and sets the text            
                    String subString = text.substring(0,index+1);
                    loadedBox.setText(subString);
                }            
            }); 
            // add the keyframe to the timeline, rinse and repeat
            timeline.getKeyFrames().add(key);
        }
        return timeline;
    }
    // shows the dialog
    public void showDialog(Dialog dialog, EventHandler<MouseEvent> onDialogFinished){
        setSpeaker(dialog.getSpeaker());
        showLine(dialog.getLine(), new EventHandler<MouseEvent>(){
            public void handle(MouseEvent event) {
                 // the event handler essentially determines whether or not to show the next text or to stop scrolling
                if(!dialog.isFinished()){ // if its not finished, keep cycling to the next dialog
                    dialog.next();
                    showDialog(dialog, onDialogFinished);
                }else{
                    onDialogFinished.handle(event); // if not, call the two handlers (the class one and the one in the method)
                    if(onFinishedHandler!=null){
                        onFinishedHandler.handle(event);
                    }
                }
            }                
        });
    }
    // shows each line, sets callback event (mouse click) for when the dialog is finished scrolling
    public void showLine(String line, EventHandler<MouseEvent> onDialogScrolled){
        Timeline timeline = generateTypingAnimation(line);
        // dont mind the amount of nested event handler anonymous classes please
        EventHandler<MouseEvent> handleInput = new EventHandler<MouseEvent>(){ // use this to handle the clicking
            public void handle(MouseEvent event){
                textBox.setOnMouseClicked(null); // disable event
                onDialogScrolled.handle(event);
            }
        };        
        // the final keyframe allows the user to interact
        KeyFrame key = new KeyFrame(Duration.millis(SCROLL_DURATION*line.length()),new EventHandler<ActionEvent>(){
            public void handle(ActionEvent event){ // handle when the keyframe is reached
                textBox.setOnMouseClicked(handleInput);
            }
        });
        timeline.getKeyFrames().add(key);
        timeline.play();
    }
    
}