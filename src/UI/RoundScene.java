package UI;

import java.util.HashMap;
import java.util.Map;

import Actors.Actor;
import Actors.Round;
import Actors.Witness;
import IO.UIController;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.*;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class RoundScene extends GameScene {
    private static final int SCENE_WIDTH = 660;

    private Timeline fadeoutAnimation;
    private Round round;
    private DialogBox dialogBox;
    private ImageView confettiIcon;
    private Map<Actor,ImageView> sprites = new HashMap<Actor,ImageView>();
    
    public RoundScene(double height, UIController uiController,int witnessCount) {
        super(SCENE_WIDTH, height, uiController);
        round = new Round(uiController,witnessCount);
        addGui();
    }
    @Override
    // add all the gui
    protected void addGui() {
       loadStyle("game-style.css");
       Group root = (Group)getRoot();
       addBackgroundImage(root); 
       addCharacterSprites(root);
       addConfettiGraphic(root);
       addDialogBox(root);
       addObjectionBox(root);
       addGraphics(root);
       addCourtRecord(root);
       addFadeOut(root);
    }
    // init the dialog box
    private void addDialogBox(Group root){
        dialogBox = new DialogBox(root);
        uiController.setDialogBox(dialogBox); 
    }
    // init the objection box
    private void addObjectionBox(Group root){
        ObjectionBox objectionBox = new ObjectionBox(root,uiController);
        uiController.setObjectionBox(objectionBox);  
    }
    private void addConfettiGraphic(Group root){
        ImageLoader confettiImage = new ImageLoader("confetti.png");
        confettiIcon = confettiImage.getView(true);
        confettiIcon.setVisible(false);
        confettiIcon.setTranslateX(70);
        confettiIcon.setTranslateY(20);
        root.getChildren().add(confettiIcon);
    }
    // initialize all of the image views for various graphics
    private void addGraphics(Group root){
        // load images
        ImageLoader testimonyImage = new ImageLoader("testimony-graphic.png");
        ImageLoader crossExamImage = new ImageLoader("crossexam-graphic.png");
         // load icons
        ImageView testimonyIcon = testimonyImage.getView(true);
        ImageView crossExamIcon = crossExamImage.getView(true);
        // set height + visible 
        crossExamIcon.setFitHeight(150);
        testimonyIcon.setFitHeight(150);
        crossExamIcon.setVisible(false);
        testimonyIcon.setVisible(false);
        // add to uiController
        uiController.setCrossExamAnimation(createTestimonyAnimation(crossExamIcon));
        uiController.setTestimonyAnimation(createTestimonyAnimation(testimonyIcon));
        // add to root
        root.getChildren().addAll(testimonyIcon,crossExamIcon);
    }
    // helper method for creating a testimony animation 
    private Timeline createTestimonyAnimation(ImageView icon){
        // create timeline
        Timeline animation = new Timeline();
        animation.setCycleCount(1);
        // initial keyframe
        KeyValue keyValue11 = new KeyValue(icon.visibleProperty(),true);
        KeyValue keyValue12 = new KeyValue(icon.xProperty(),-300);        
        KeyFrame keyFrame1 = new KeyFrame(Duration.millis(0),keyValue11,keyValue12);

        // second keyframe
        KeyValue keyValue21 = new KeyValue(icon.visibleProperty(),true);
        KeyValue keyValue22 = new KeyValue(icon.xProperty(), 150);        
        KeyFrame keyFrame2 = new KeyFrame(Duration.millis(200),keyValue21,keyValue22);

        //final keyframe
        KeyValue keyValue31 = new KeyValue(icon.visibleProperty(),false);  
        KeyFrame keyFrame3 = new KeyFrame(Duration.millis(1200),keyValue31);

        animation.getKeyFrames().addAll(keyFrame1,keyFrame2,keyFrame3);
        return animation;
    }    
    // initialize character sprites
    private void addCharacterSprites(Group root){
        // create a sprite for the player
        sprites.put(round.getPlayer(),getSprite(round.getPlayer(),root));
        // loop thru witnesses
        for(Witness witness: round.getWitnesses()){
            // add an imageview for each witness and their icon 
            ImageView view = getSprite(witness,root);
            sprites.put(witness,view); 
        }
    } 
    // get the sprite data from an actor
    private ImageView getSprite(Actor actor, Group root){
        ImageView sprite = actor.getIcon().getView(true);
        sprite.setTranslateX(120);
        sprite.setTranslateY(10);
        sprite.setVisible(false);
        root.getChildren().add(sprite);
        return sprite;
    }     
    // init the court record box
    private void addCourtRecord(Group root){
        CourtRecordBox courtRecordBox = new CourtRecordBox(root,round.getCourtRecord());    
    }
    // add a background image to the scene
    private void addBackgroundImage(Group container){ 
        ImageLoader imageLoader = new ImageLoader("round-bg.png");
        addBackgroundImage(container,imageLoader);
    }
    // adds a black rectangle that fades out 
    private void addFadeOut(Group container){
        Rectangle fadeOutBox = new Rectangle(getWidth(),getHeight()); 
        fadeOutBox.setVisible(false); // set invisible in case it messed up collision
        fadeOutBox.setFill(Color.BLACK);
        // setup animation
        fadeoutAnimation = new Timeline();
        fadeoutAnimation.setCycleCount(1);
        // initial keyframe
        KeyValue kv1 = new KeyValue(fadeOutBox.opacityProperty(),0); 
        KeyValue kv12 = new KeyValue(fadeOutBox.visibleProperty(),true);
        KeyFrame kf1 = new KeyFrame(Duration.millis(0),kv1,kv12);
        // second keyframe
        KeyValue kv2 = new KeyValue(fadeOutBox.opacityProperty(),1); 
        KeyFrame kf2 = new KeyFrame(Duration.millis(1200),kv2,kv12);
        // third keyframe, the game switches to the new scene
        KeyFrame kf3 = new KeyFrame(Duration.millis(2000),(event)->{
            RoundEndScene roundEndScene = new RoundEndScene(getWidth(), getHeight(), uiController, uiController.getHasWon());
            uiController.getSceneController().showNewScene(roundEndScene);
        });
        fadeoutAnimation.getKeyFrames().addAll(kf1,kf2,kf3);
        container.getChildren().add(fadeOutBox);
    }
    // starts the round for the game
    private void startRound(){
        round.introduction();
        dialogBox.setOnFinishedDialog(new TestimonyHandler(round,uiController));        
    }  
    // shows a sprite
    public void showSprite(Actor actor){
        // first hide all sprites
        for(ImageView sprite: sprites.values()){
            sprite.setVisible(false);
        }
        // then get the hash and set it to visible
        sprites.get(actor).setVisible(true);
    }
    // Reduces the brightness of a sprite
    public void darkenSprite(Actor actor){
        ColorAdjust blackout = new ColorAdjust();
        blackout.setBrightness(-0.5);
        sprites.get(actor).setEffect(blackout);
    }
    // Shows the confetti image
    public void showConfetti(){
        confettiIcon.setVisible(true);
    }
    public void fadeOut(){
        fadeoutAnimation.play();
    }
    @Override
    public void show() {
        startRound();     
    }

}
