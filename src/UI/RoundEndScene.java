package UI;

import IO.UIController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class RoundEndScene extends GameScene {
    private boolean hasWon; 
    public RoundEndScene(double width, double height, UIController uiController, boolean hasWon) {
        super(width, height, uiController);
        this.hasWon = hasWon;
        addGui();
    }
    // adds the gui to the scene
    @Override
    protected void addGui() {
        Group root = (Group)getRoot();
        // add the vbox container
        VBox container = new VBox();
        container.setPrefSize(getWidth(), getHeight());
        container.setSpacing(10);
        container.setAlignment(Pos.CENTER); 
        container.setPadding(new Insets(10,0,10,0));
        // add the children nodes inside the container 
        addTitle(container);
        addRestartButton(container);    
        // parent the container 
        root.getChildren().add(container);   
    }
    // add a title to the scene, telling the user if they lost or won the round
    private void addTitle(VBox container){
        String text = hasWon? "VICTORY!" : "YOU FAILED.";
        Label titleText = new Label(text);  
        titleText.setTextFill(Color.WHITE);   
        titleText.setFont(new Font("Serif",40));        
        container.getChildren().add(titleText);
    } 
    // add a button that takes the user to the input scene
    private void addRestartButton(VBox container){
        // the restart button removes the last roundscene and takes the player back to the input name scene
        Button button = new Button("Restart");
        button.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent event){
                // get the scene controller, remove the last round scene and add a new input name scene
                SceneController sceneController = uiController.getSceneController();
                sceneController.removeScene("RoundScene");
                sceneController.showScene("InputNameScene");
            }
        });
        container.getChildren().add(button);
    }
    // show the scene
    @Override
    public void show() {
        setFill(Color.BLACK);        
    }
    
}
