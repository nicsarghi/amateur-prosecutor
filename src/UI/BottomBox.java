package UI;

import javafx.scene.Group;
import javafx.scene.layout.Region;
// bottom boxes are used for the objection/dialog box
// just initializes a height + width and a position
public abstract class BottomBox extends Region  {
    private final int WIDTH = 600;
    private final int HEIGHT = 150;
    private final int OFFSET_X = 30;
    private final int OFFSET_Y = 350;

    public BottomBox(Group container){
        setWidth(WIDTH);
        setHeight(HEIGHT);
        container.getChildren().add(this);
        setTranslateX(OFFSET_X);
        setTranslateY(OFFSET_Y);
    }

    // adds all relevant gui elements inside the box
    protected abstract void addGui();
}
