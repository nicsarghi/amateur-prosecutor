package UI;

import CourtRecord.Evidence;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class CourtRecordBox extends Region {
    private final int HEIGHT = 500;
    private final int WIDTH = 500;
    public CourtRecordBox(Group root,Evidence[] courtRecord){
        super();
        root.getChildren().add(this);
        drawButton();
        drawBox(courtRecord);
    }
    private void drawButton(){
        Button show = new Button("Show court record");
        show.setTranslateX(450);
        CourtRecordBox courtRecord = this;
        show.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                courtRecord.setVisible(!courtRecord.isVisible());                  
                show.setText(courtRecord.isVisible()?"Hide court record":"Show court record"); 
            }            
        });
        ((Group)getParent()).getChildren().add(show);
    }
    private void drawBox(Evidence[] courtRecord){
        setVisible(false);
        setWidth(WIDTH);
        setHeight(HEIGHT);
        getStyleClass().add("court-record");
        // add vbox container
        VBox vbox = new VBox();
        vbox.setSpacing(8);
        vbox.setPadding(new Insets(8,4,8,4));
        getChildren().add(vbox);
        // add court record
        for(Evidence evidence: courtRecord){
            Label label = drawEvidenceLabel(evidence);
            vbox.getChildren().add(label);
        }
    }
    private Label drawEvidenceLabel(Evidence evidence){
        Label label = new Label(evidence.getType()+" : "+evidence.getValue());
        label.setPrefWidth(WIDTH);
        label.setTextFill(Color.WHITE);
        label.getStyleClass().add("court-record-label");
        return label;
    }
}
