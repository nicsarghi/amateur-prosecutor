package UI;
import Actors.StatementType;
import IO.UIController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
// event handler class for pressing an objection button
public class ObjectionActionHandler implements EventHandler<ActionEvent>{
    private StatementType statementType; 
    private UIController uiController;
    // sets the statement type and controller
    public ObjectionActionHandler(StatementType statementType, UIController uiController){
        this.statementType = statementType;
        this.uiController = uiController;
    }
    @Override
    public void handle(ActionEvent event) {
        // prompt the player to retort with their own dialog
        uiController.getRound().getPlayer().objectProposal(statementType);
        // set the next objection
        uiController.setNextObjection(statementType);        
    }            
} 