package IO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

import UI.ImageLoader;

// no extend because it does its own thing
public class CharacterSpriteReader {
    private ArrayList<ImageLoader> sprites;
    private Random randomizer;
    // when instantiated, gets all images from the file
    public CharacterSpriteReader(){
        try{
            randomizer = new Random();
            // get the file stream
            Stream<Path> filesInFolder = Files.walk(Paths.get("src/Assets/CharacterSprites")).filter(Files::isRegularFile); 
            generateSpriteList(filesInFolder);
            // check if loaded
            if(sprites.size()==0){
                throw new IOException("No image files found in character sprites folder");
            }
        }catch(IOException exception){
            exception.printStackTrace();
            throw new IllegalStateException("Could not instantiate CharacterSpriteReader: "+exception.getMessage());
        }
    }
    private void generateSpriteList(Stream<Path> filesInFolder){
        // create the sprites arraylist
        sprites = new ArrayList<ImageLoader>();
        filesInFolder.forEach(path->{    
            try{
                // get mimetype, throw ioexception if its not nil  
                String mimetype= Files.probeContentType(path);
                if (mimetype == null) {
                    throw new IOException("Not an image: "+path.toString());
                }
                // check if its an image
                String type = mimetype.split("/")[0];
                if(!type.equals("image")){
                    throw new IOException("Not an image: "+path.toString());
                }                    
                sprites.add(new ImageLoader("CharacterSprites/"+path.getFileName().toString()));
            }
            catch(IOException exception){
                exception.printStackTrace();
                throw new IllegalStateException("Could not evaluate mimetype: "+exception.getMessage());
            }
        });
    }
    public ImageLoader getRandomSprite(){
        return sprites.get(randomizer.nextInt(sprites.size()));
    }    
}
