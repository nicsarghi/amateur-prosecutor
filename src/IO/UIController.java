package IO;

import Actors.Actor;
import Actors.Dialog;
import Actors.StatementType;
import Actors.Witness;
import UI.DialogBox;
import UI.ObjectionBox;
import UI.RoundScene;
import UI.SceneController;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

// player controller class but used for the ui
public class UIController extends PlayerController {
    private TextField input;
    private DialogBox dialogBox;
    private ObjectionBox objectionBox;
    private Timeline crossExamAnimation;
    private Timeline testimonyAnimation;
    private SceneController sceneController;
    private boolean hasWon;
     //sets the player name
    public void prepareName(){
        this.name = input.getText();
    }
    // set the current inputfield
    public void setInput(TextField input){
        this.input = input;
    }
    // setter for dialog box
    public void setDialogBox(DialogBox dialogBox){
        this.dialogBox = dialogBox;
    }
    // setter for objection box
    public void setObjectionBox(ObjectionBox objectionBox){
        this.objectionBox = objectionBox;
    }
    // getter for scene controller
    public SceneController getSceneController() {
        return sceneController;
    }
    // scene controller setter
    public void setSceneController(SceneController sceneController) {
        this.sceneController = sceneController;
    } 
    //testimony animation setter
    public void setTestimonyAnimation(Timeline testimonyAnimation) {
        this.testimonyAnimation = testimonyAnimation;     
    }
    // cross exam animation setter
    public void setCrossExamAnimation(Timeline crossExamAnimation) {
        this.crossExamAnimation = crossExamAnimation;
    }
    // hasWon getter, should be called when the round has finished, 
    //otherwise it'll always return false
    public boolean getHasWon(){
        return hasWon;
    }
    // hides the objection box and enables the dialog box
    public void setNextObjection(StatementType nextStatement){
        this.nextStatement = nextStatement;
    }

    // Displays the dialog box
    public void showDialog(Dialog dialog){
        dialogBox.setVisible(true);
        objectionBox.setVisible(false); // hide the objection box incase it was open
        RoundScene roundScene = (RoundScene)sceneController.getCurrentScene();  
        // show actor sprite
        if (dialog.getActor()!=null){
            roundScene.showSprite(dialog.getActor());
        }
        // show dialog
        dialogBox.showDialog(dialog,new EventHandler<MouseEvent>() {
            public void handle(MouseEvent arg0) {
                dialogBox.setVisible(false);             
            }    
        });
    }   
    // prompt the player to object by opening the objection box and hiding the dialog
    @Override
    public void prepareObjection(){
        objectionBox.setVisible(true);
        dialogBox.setVisible(false); 
    }
    // show the cross exam graphic by playing the animation 
    @Override
    public void showCrossExaminationGraphic() {
        this.crossExamAnimation.play();
    }
    // show the testimony graphic by playing the animation 
    @Override
    public void showTestimonyGraphic() {
        this.testimonyAnimation.play();        
    }
    // show the finishing sequence of the game
    @Override
    public void finishingSequence(boolean hasWon, Witness lastWitness) {  
        // set who has won  
        this.hasWon = hasWon;
        RoundScene roundScene = (RoundScene)sceneController.getCurrentScene();   
        // darken the "loser" sprite, aka the last witness or player 
        super.finishingSequence(hasWon, lastWitness);
        Actor spriteToDarken = hasWon ? lastWitness : round.getPlayer();
        roundScene.darkenSprite(spriteToDarken); 
        roundScene.showSprite(spriteToDarken);       
        // show the confetti if the player wins
        if(hasWon){
            roundScene.showConfetti();
        }
    }  
    // ends the round by fading out the screen (and switching scenes subsequently)
    public void endRound(){
        RoundScene roundScene = (RoundScene)sceneController.getCurrentScene();
        roundScene.fadeOut();
    }
}
