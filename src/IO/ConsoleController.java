package IO;

import java.util.Scanner;

import Actors.Dialog;
import Actors.StatementType;
import Actors.Witness;
import CourtRecord.Evidence;
// controller class for using the console output  
public class ConsoleController extends PlayerController {
    private Scanner scanner; 
    // create a scanner that gets input
    public ConsoleController(){
        super();
        this.scanner = new Scanner(System.in);
    }
    // prompts the player to get the name through scanner
    @Override
    public void prepareName() {
        System.out.print("Your name: ");
        this.name = scanner.next();
    }
    // shows dialog to the console
    @Override
    public void showDialog(Dialog dialog) {
        do{
            // insert the name to the dialog
            String speaker = dialog.getSpeaker() == null ? "" : dialog.getSpeaker()+": ";
            // speak each line until there's no more
            System.out.println(speaker+dialog.getLine());
            dialog.next();
        }while(!dialog.isFinished());
    }
    // prompts the player to type in their objection
    @Override
    public void prepareObjection() {
        System.out.print("Your proposal--");
        String proposal = scanner.next().toLowerCase();
        switch (proposal) {
            case "lie":
                nextStatement = StatementType.Lie;
                break;        
            case "truth":
                nextStatement = StatementType.Truth;
                break;
            default:
                nextStatement = StatementType.HalfTruth;   
                break; 
        }   
    }
    // show the cross examination text
    @Override
    public void showCrossExaminationGraphic() {
        System.out.println("- CROSS EXAMINATION - ");        
    }
    // show the testimony text 
    @Override
    public void showTestimonyGraphic() {
        System.out.println("- TESTIMONY -");
        
    }
    // output the finishing sequence dialog
    @Override
    public void finishingSequence(boolean hasWon, Witness lastWitness) {
        super.finishingSequence(hasWon, lastWitness);        
    }
    // Displays the court record to the screen
    public void showCourtRecord(){
        // showing the court record SHOULD NOT be a dialog.
        for(Evidence evidence: round.getCourtRecord()){ // list the court record.
            System.out.println(evidence.getType()+": "+evidence.getValue());
        }
    }
}
