package IO;

import java.io.IOException;
import java.util.ArrayList;

public class CharacterNameReader extends GameFileReader {
    private ArrayList<String> takenNames;

    // Initialize taken names and read the file
    public CharacterNameReader(){
        super("src/Files/NameList.txt");
        takenNames = new ArrayList<String>(); // the taken names is an empty list    
    }
    // Try to get a random name from a random index. If it's taken, retry
    private String tryGetRandomName() throws IOException{
        int maxRepititions = fileData.size(); // you want to retry until theres no more names left 
        int repetitions = 0; // keep track of reps
        do {            
            String randomName = getRandomArgument(fileData)[0];
            if(!takenNames.contains(randomName)){ // if the name isnt taken, add it to the list and return it 
                takenNames.add(randomName);
                return randomName;
            }
            repetitions++;
        } while(repetitions<maxRepititions); // prevents an infinite loop
        throw new IOException("No names available");        
    }
    // Gets a random name from the list of names in the NameList file
    public String getRandomCharacterName(){
        try {
            return tryGetRandomName();
        } catch (IOException exeception) {
            throw new IllegalStateException("Could not get random name: "+exeception.getMessage());
        }
    }
}
