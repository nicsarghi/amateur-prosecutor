package IO;
import java.io.IOException;

public class DialogTemplateReader extends GameFileReader {
    // Initialize constructor
    public DialogTemplateReader(){
        super("src/Files/DialogTemplate.txt");
    }
    // Try to get a random template from the evidence type. Replace the values inside the main value with toReplace
    // Return the formatted string with the replaced value 
    public String getRandomTemplate(String evidenceType, String toReplace){
        try{
            // format: [0] = statement, [1]=evidenceType
            String[] arguments = getRandomArgument(evidenceType, 1, fileData);
            // then format the first argument by replacing %VALUE% before returning it
            String replaced = arguments[0].replaceFirst("%VALUE%",toReplace);
            return replaced;  

        }catch(IOException exception){
            throw new IllegalArgumentException("Could not find template for "+evidenceType+", reason: "+exception.getMessage());
        }
    } 
}

