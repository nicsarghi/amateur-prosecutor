package IO;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class GameFileReader { // abstract class as to not create an instance of it
    private Random randomizer;
    protected ArrayList<String[]> fileData;
    public GameFileReader(String fileName){
        try{
            // initialize the randomizer and the file reader
            fileData = readFile(fileName);
            randomizer = new Random();  
        }catch(IOException exception){
            throw new IllegalStateException("Cannot retrieve file for reader");
        } 
    }
    // Read a file from given file path. Splits each line by parsing it.
    protected ArrayList<String[]> readFile(String fileName) throws IOException{
        Path path = Paths.get(fileName);
        List<String> fileLines = Files.readAllLines(path); // read all lines
        // go through each line, parse it and populate the array list with it 
        ArrayList<String[]> parsedLines = new ArrayList<String[]>();
        for(String line: fileLines){
            parsedLines.add(parse(line));
        }
        return parsedLines;
    }
    // Parses each line and returns an array of Strings based on the split result 
    private String[] parse(String line){
        return line.split(";");
    }
    // Filters an arraylist based on the argument filter and the index for that argument
    // Note that an argument is each entry in a string array
    protected ArrayList<String[]> filterBasedOnArgument(ArrayList<String[]> initialList, String filter, int argumentIndex){
        ArrayList<String[]> filteredList = new ArrayList<String[]>();
        // Iterate through the initial list, and match with the regexp filter
        for(String[] arguments: initialList){
            // add to filtered list if the indexed argument is matched with the filter
            if(arguments[argumentIndex].matches(filter)){
                filteredList.add(arguments);
            }
        }
        return filteredList;
    }
    // Gets a random argument, returns it
    protected String[] getRandomArgument(ArrayList<String[]> initialList){
        // check size
        if(initialList.size()==0){
            throw new IllegalArgumentException("Provided list is empty");
        }
        // get a random index and return it
        int randomIndex = randomizer.nextInt(initialList.size());
        return initialList.get(randomIndex);             
    }
    // Overloaded method for retrieving the random argument by filtering it. 
    protected String[] getRandomArgument(String filter, int argumentIndex, ArrayList<String[]> initialList) throws IOException{        
        // filter the initial list first
        ArrayList<String[]> filteredList = filterBasedOnArgument(initialList, filter, argumentIndex);
        try{
            // get a random argument from the filtered list
            String[] randomArgument = getRandomArgument(filteredList );
            return randomArgument;
        }catch(IllegalArgumentException exception){
            throw new IOException("No existing match for filter "+filter.toString()+", arg: "+argumentIndex);
        }
    }
 
}