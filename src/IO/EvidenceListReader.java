package IO;
import java.io.IOException;

public class EvidenceListReader extends GameFileReader {
    public EvidenceListReader(){
        super("src/Files/Evidence.txt");
    }
    // Gets a string array representing a line in the evidence list. Retrieves one randomly 
    public String[] getRandomEvidenceArguments(String evidenceType){
        // Evidence argument format: [0]=evidenceValue, [1]=evidenceType, [2...n] any
        try{
            String[] argument = getRandomArgument(evidenceType, 1, fileData);
            return argument; // return the evidence    
        }catch(IOException exception){
            throw new IllegalArgumentException("Could not retrieve random evidence of type "+evidenceType+", "+exception.getMessage());
        }     
    }
    // Wrapper for getRandomEvidenceArguments. Returns the first argument (the evidence name) 
    public String getRandomEvidence(String evidenceType){               
        return getRandomEvidenceArguments(evidenceType)[0];   
    } 
}
