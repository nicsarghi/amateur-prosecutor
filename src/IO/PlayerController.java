package IO;

import Actors.Dialog;
import Actors.Round;
import Actors.StatementType;
import Actors.Witness;

//serves as the controller between the back end and front end
public abstract class PlayerController {
    protected String name;
    protected Round round;
    protected StatementType nextStatement;

    // returns the name field
    public String getName(){
        //System.out.println("Input your name:");
        return name;
    }
    // triggers the controller to take in input for the name
    public abstract void prepareName();

    // round setter
    public void setRound(Round round) {
        this.round = round;
    }
    // round getter
    public Round getRound() {
        return round;
    }
    // shows dialog to the console/screen
    public abstract void showDialog(Dialog dialog);

    // Prompts the player to enter their proposal to a witness' testimony
    public StatementType getObjection(){
        StatementType objection = nextStatement;
        //nextStatement = null; // "empty" the next objection
        return objection;
    }
    // prompt a player to input their objection
    public abstract void prepareObjection(); 
    // shows the cross examination icon
    public abstract void showCrossExaminationGraphic();   
    // shows the testimony icon
    public abstract void showTestimonyGraphic();   
    // finishing sequence dialog for the game
    public void finishingSequence(boolean hasWon, Witness lastWitness){
        // get dialog based on who won and output to the screen
        String[] winLines = hasWon? new String[]{"You finally found the true culprit of the case, and the verdict for your defendant is declared not guilty. "+lastWitness.getName()+" stares on in horror as confetti flies everywhere."} : new String[]{"You have lost the case. The true culprit may never be found and your defendant is delcared guilty."};
        showDialog(new Dialog(winLines));
    }

}
